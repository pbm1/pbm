
LD_LIBRARY_PATH="/opt/alfresco-community/postgresql/lib:/opt/alfresco-community/common/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH
DYLD_FALLBACK_LIBRARY_PATH="/opt/alfresco-community/postgresql/lib:/opt/alfresco-community/common/lib:$DYLD_FALLBACK_LIBRARY_PATH"
export DYLD_FALLBACK_LIBRARY_PATH
LC_MESSAGES=C
export LC_MESSAGES
        

PGHOST=localhost
export PGHOST
PGPORT=5432
export PGPORT
PGDATA="/opt/alfresco-community/alf_data/postgresql"
export PGDATA
        