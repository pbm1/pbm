# Load Tomcat Native Library
LD_LIBRARY_PATH=/opt/alfresco-community/common/lib:$LD_LIBRARY_PATH

JAVA_HOME=/opt/alfresco-community/java
JRE_HOME=$JAVA_HOME
JAVA_OPTS="-XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -Djava.awt.headless=true -Dalfresco.home=/opt/alfresco-community -XX:ReservedCodeCacheSize=128m $JAVA_OPTS "
JAVA_OPTS="-Xms512M -Xmx3910M $JAVA_OPTS " # java-memory-settings
export JAVA_HOME
export JRE_HOME
export JAVA_OPTS
export LD_LIBRARY_PATH
                