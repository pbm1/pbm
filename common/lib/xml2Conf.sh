#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-L/bitnami/alfrescocustom42stack-linux-x64/output/common/lib"
XML2_LIBS="-lxml2 -L/bitnami/alfrescocustom42stack-linux-x64/output/common/lib -lz     -liconv  -lm "
XML2_INCLUDEDIR="-I/bitnami/alfrescocustom42stack-linux-x64/output/common/include/libxml2"
MODULE_VERSION="xml2-2.9.4"

