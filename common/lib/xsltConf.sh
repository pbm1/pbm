#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/bitnami/alfrescocustom42stack-linux-x64/output/common/lib"
XSLT_LIBS="-lxslt  -L/bitnami/alfrescocustom42stack-linux-x64/output/common/lib -lxml2 -L/bitnami/alfrescocustom42stack-linux-x64/output/common/lib -lz -liconv -lm -ldl -lm -lrt"
XSLT_INCLUDEDIR="-I/bitnami/alfrescocustom42stack-linux-x64/output/common/include"
MODULE_VERSION="xslt-1.1.29"
