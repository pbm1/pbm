#!/bin/sh
LDFLAGS="-L/opt/alfresco-community/common/lib $LDFLAGS"
export LDFLAGS
CFLAGS="-I/opt/alfresco-community/common/include $CFLAGS"
export CFLAGS
CXXFLAGS="-I/opt/alfresco-community/common/include $CXXFLAGS"
export CXXFLAGS
            
PKG_CONFIG_PATH="/opt/alfresco-community/common/lib/pkgconfig"
export PKG_CONFIG_PATH
