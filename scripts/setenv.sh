#!/bin/sh
echo $LD_LIBRARY_PATH | egrep "/opt/alfresco-community/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/opt/alfresco-community/common/alfresco-pdf-renderer:/opt/alfresco-community/java/bin:/opt/alfresco-community/postgresql/bin:/opt/alfresco-community/common/bin:$PATH"
export PATH
LD_LIBRARY_PATH="/opt/alfresco-community/postgresql/lib:/opt/alfresco-community/common/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH
fi

TERMINFO=/opt/alfresco-community/common/share/terminfo
export TERMINFO
##### ALFRESCO_PDF_RENDERER ENV #####
ALFRESCO_PDF_RENDERER_VAR="/opt/alfresco-community/common/alfresco-pdf-renderer"
export ALFRESCO_PDF_RENDERER_VAR

##### IMAGEMAGICK ENV #####
MAGICK_HOME="/opt/alfresco-community/common"
export MAGICK_HOME
MAGICK_CONFIGURE_PATH="/opt/alfresco-community/common/lib/ImageMagick-7.0.5/config-Q16HDRI"
export MAGICK_CONFIGURE_PATH
MAGICK_CODER_MODULE_PATH="/opt/alfresco-community/common/lib/ImageMagick-7.0.5/modules-Q16HDRI/coders"
export MAGICK_CODER_MODULE_PATH

##### JAVA ENV #####
JAVA_HOME=/opt/alfresco-community/java
export JAVA_HOME

##### POSTGRES ENV #####

        ##### SSL ENV #####
SSL_CERT_FILE=/opt/alfresco-community/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE
OPENSSL_CONF=/opt/alfresco-community/common/openssl/openssl.cnf
export OPENSSL_CONF
OPENSSL_ENGINES=/opt/alfresco-community/common/lib/engines
export OPENSSL_ENGINES


. /opt/alfresco-community/scripts/build-setenv.sh
